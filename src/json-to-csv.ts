import jsonexport from "jsonexport";
import fs from "fs";
import { Datum } from "./search-results.model";

const main = async () => {
  const salariedJobs: Datum[] = JSON.parse(
    fs.readFileSync("./data/salaried.json").toString()
  );
  const csv = await jsonexport(
    salariedJobs
      .map((job) => {
        const match = /\$(\d+).*-.*\$(\d+)/.exec(job.salary);
        if (!match) return console.error("Couldn't parse salary of job", job);
        let salary = (Number(match[1]) + Number(match[2])) / 2;
        if (salary > 1000) salary /= 1000;
        return {
          ...job,
          salary,
        };
      })
      .filter((job) => job),
    {}
  );
  fs.writeFileSync("./data/salaried-jobs.csv", csv);
};

main().then();
