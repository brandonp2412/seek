export interface Advertiser {
  id: string;
  description: string;
}

export interface Logo {
  id: string;
  description: string;
}

export interface Classification {
  id: string;
  description: string;
}

export interface SubClassification {
  id: string;
  description: string;
}

export interface Tags {
  "mordor:flights": string;
}

export interface SolMetadata {
  searchRequestToken: string;
  jobId: string;
  section: string;
  sectionRank: number;
  jobAdType: string;
  tags: Tags;
}

export interface Strategies {
  jdpLogo: string;
  serpLogo: string;
}

export interface Logo2 {
  id: string;
  description: string;
  url: string;
  strategies: Strategies;
}

export interface Strategies2 {
  jdpCover: string;
  jdpCoverThumbnail: string;
}

export interface Cover {
  id: string;
  url: string;
  strategies: Strategies2;
}

export interface Assets {
  logo: Logo2;
  cover: Cover;
}

export interface Logo3 {
  url: string;
}

export interface Branding {
  assets: Assets;
  id: string;
  logo: Logo3;
}

export interface Datum {
  id: number;
  listingDate: Date;
  title: string;
  teaser: string;
  bulletPoints: string[];
  advertiser: Advertiser;
  logo: Logo;
  isPremium: boolean;
  isStandOut: boolean;
  location: string;
  locationId: number;
  area: string;
  areaId: number;
  workType: string;
  classification: Classification;
  subClassification: SubClassification;
  salary: string;
  companyProfileStructuredDataId: number;
  locationWhereValue: string;
  areaWhereValue: string;
  automaticInclusion: boolean;
  displayType: string;
  tracking: string;
  joraClickTrackingUrl: string;
  joraImpressionTrackingUrl: string;
  solMetadata: SolMetadata;
  branding: Branding;
  isPrivateAdvertiser: boolean;
  suburbId?: number;
  suburbWhereValue: string;
  templateFileName: string;
  roleId: string;
  companyName: string;
}

export interface Location {
  matched: boolean;
  description: string;
  whereId: number;
  locationId: number;
  locationDescription: string;
  areaId: number;
  areaDescription: string;
  type: string;
  stateDescription: string;
  suburbType: string;
  suburbParentDescription: string;
}

export interface PaginationParameters {
  seekSelectAllPages: boolean;
  hadPremiumListings: boolean;
}

export interface Info {
  timeTaken: number;
  source: string;
  experiment: string;
}

export interface SortMode {
  name: string;
  value: string;
  isActive: boolean;
}

export interface Tags2 {
  "mordor:flights": string;
  "seek:listviewid": string;
  "chalice-search-api:solId": string;
}

export interface SolMetadata2 {
  requestToken: string;
  keywords: string;
  locations: string[];
  pageSize: number;
  pageNumber: number;
  totalJobCount: number;
  tags: Tags2;
}

export interface JoraCrossLink {
  canCrossLink: boolean;
}

export interface SearchParams {
  sitekey: string;
  sourcesystem: string;
  userqueryid: string;
  userid: string;
  usersessionid: string;
  eventcapturesessionid: string;
  where: string;
  page: string;
  seekselectallpages: string;
  keywords: string;
  include: string;
  solid: string;
}

export interface SearchResults {
  title: string;
  totalCount: number;
  data: Datum[];
  location: Location;
  paginationParameters: PaginationParameters;
  info: Info;
  userQueryId: string;
  sortMode: SortMode[];
  solMetadata: SolMetadata2;
  joraCrossLink: JoraCrossLink;
  searchParams: SearchParams;
}
