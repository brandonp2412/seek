import fetch from "node-fetch";
import { Datum, SearchResults } from "./search-results.model";
import fs from "fs";
import jsonexport from "jsonexport";

const searchUrl = "https://www.seek.co.nz/api/chalice-search/search";
const siteKey = "NZ-Main";
const where = "All Auckland";
const keywords = "Software Developer";

const search = async (page: number): Promise<SearchResults> => {
  const response = await fetch(
    `${searchUrl}?siteKey=${siteKey}&where=${where}&page=${page}&keywords=${keywords}`,
    {
      headers: {
        accept: "application/json, text/plain, */*",
      },
      method: "GET",
    }
  );
  return response.json();
};

const salariedResults: Datum[] = [];

const main = async () => {
  const firstPage = await search(1);
  salariedResults.push(
    ...firstPage.data.filter((result) => result.salary?.includes("$"))
  );
  const limit = 22;
  for (let page = 2; page < Math.ceil(firstPage.totalCount / limit); page++) {
    console.log(`Getting page ${page}/${firstPage.totalCount / limit}`);
    const currentPage = await search(page);
    salariedResults.push(
      ...currentPage.data.filter((result) => result.salary?.includes("$"))
    );
  }
  const csv = await jsonexport(salariedResults, {});
  fs.writeFileSync("./data/salaried.csv", csv);
};

main().then();
